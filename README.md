Role Name
=========

A role that installs min.io, <https://min.io/>.
The supported installation mode is *baremetal* and *distributed*.
minio is not installed from a package, but the binary is downloaded and placed into `/usr/local/bin`.

Set the `minio_upgrade_executable` to `True` if you want to upgrade an existing installation.

Role Variables
--------------

The most important variables are listed below:

``` yaml
minio_baremetal: true
minio_enabled: true
minio_binary: 'minio'
minio_binary_download: 'https://dl.min.io/server/minio/release/linux-amd64/{{ minio_binary }}'
minio_download_validate_certs: true
minio_upgrade_executable: false
minio_work_dir: /usr/local
minio_install_dir: '{{ minio_work_dir }}/bin'
minio_executable: '{{ minio_install_dir }}/{{ minio_binary }}'
minio_username: 'minio-user'
minio_user_home: '/srv/{{ minio_username }}'
minio_access_key: 'use a vault'
minio_secret_key: 'use a vault'
minio_secrets:
  - {name: minio_access_key, data: '{{ minio_access_key }}'}
  - {name: minio_secret_key, data: '{{ minio_secret_key }}'}
minio_server_instances_num: 4
minio_server_name_prefix: 'minio'
minio_server_domain_name: 'example.org'
minio_disk_volumes: 4
minio_disk_volume_names:
  - 1
  - 2
  - 3
  - 4

minio_data_prefix: /storage
minio_volume_prefix: 'minio'
minio_volume_subdir: 'data'
minio_port: 9000
minio_volumes: 'https://{{ minio_server_name_prefix }}{%raw%}{{%endraw%}1...{{ minio_server_instances_num }}{%raw%}}{%endraw%}.{{ minio_server_domain_name }}:{{ minio_port }}{{ minio_data_prefix }}/{{ minio_volume_prefix }}{%raw%}{{%endraw%}1...{{ minio_disk_volumes }}{%raw%}}{%endraw%}/{{ minio_volume_subdir }}'

minio_dedicated_console: true
minio_console_port: 9001
minio_behind_haproxy: true
minio_server_url: 'https://minio-reverse-proxy.example.org'
minio_ui_url: 'https://minio-ui-reverse-proxy.example.org'
minio_over_tls: true
minio_letsencrypt_certs: true
minio_tls_certs_dir: /etc/pki/minio
# The certificate and private key file names
# must be *exactly* the ones used here.
minio_tls_cert_file: '{{ minio_tls_certs_dir }}/public.crt'
minio_tls_key_file: '{{ minio_tls_certs_dir }}/private.key'
minio_root_user: minio_adm
# minio_root_password: 'Use a vault'
minio_storage_class_standard: 4
minio_storage_class_rrs: 2
#
minio_prometheus_url: 'https://prometheus.localhost'
minio_prometheus_jobid: 'minio-job'
minio_prometheus_auth_type: public
minio_external_oidc: false
minio_openid_config_url: http://localhost:8080/auth/
minio_openid_realm: 'realm'
minio_openid_client_id: 'minio_client_id'
# minio_openid_client_secret: 'use a vault'
minio_openid_claim_name: 'policy'
minio_openid_set_claim_prefix: false
minio_openid_claim_prefix: 'minio_'
minio_openid_scopes: ''
minio_openid_redirect_uri: '{{ minio_ui_url }}/oauth_callback'
```

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
